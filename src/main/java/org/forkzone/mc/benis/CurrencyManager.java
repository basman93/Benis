package org.forkzone.mc.benis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CurrencyManager
{
	private Map<String, Double> playerbenis;
	private String currencyName;
	private String currencySymbol;
	private double minValue;
	private double minUnit;
	private double maxValue;
	private double maxUnit;
	private double defaultStartValue;
	private double amount;
	private int minimalDecimal;
	private String message;

	CurrencyManager(String currencyName, String currencySymbol, double minValue, double maxValue, double minUnit, double maxUnit, double defaultStartValue, int minimalDecimal)
	{
		this.currencyName = currencyName;
		this.currencySymbol = currencySymbol;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.minUnit = minUnit;
		this.maxUnit = maxUnit;
		this.defaultStartValue = defaultStartValue;
		this.minimalDecimal = minimalDecimal;
		this.message = "";
		this.playerbenis = new HashMap<String, Double>();
	}

	public static String playerToUID(Player p)
	{
		return p.getUniqueId().toString();
	}

	public static String playerToUID(CommandSender s)
	{
		Player p = (Player) s;
		return p.getUniqueId().toString();
	}

	public double getPlayerCurrency(String uid)
	{
		return playerbenis.containsKey(uid) ? playerbenis.get(uid) : 0;
	}

	public ArrayList<String> getPlayerList()
	{
		return new ArrayList<String>(playerbenis.keySet());
	}

	public void setPlayerCurrency(String uid, double currency)
	{
		amount = currency;
		playerbenis.put(uid, currency);
	}

	public void addPlayerCurrency(String uid, double currency)
	{
		amount = currency;
		if(getPlayerCurrency(uid) + currency <= maxValue)
			playerbenis.put(uid, getPlayerCurrency(uid) + currency);
	}

	public boolean existsPlayer(String uid)
	{
		return playerbenis.containsKey(uid);
	}

	public void addPlayer(String uid)
	{
		playerbenis.put(uid, defaultStartValue);
	}

	public void deletePlayer(String uid)
	{
		playerbenis.remove(uid);
	}

	public void deleteAllPlayer()
	{
		playerbenis.clear();
	}

	public boolean transfarCurrency(String senduid, String reciveuid, double amount)
	{
		this.amount = Math.round(amount * Math.pow(10, minimalDecimal)) / Math.pow(10, minimalDecimal);
		if(this.amount < minUnit)
		{
			message = "is not enough to send.";
			return false;
		}
		else if(this.amount > maxUnit)
		{
			message = "is too much to send.";
			return false;
		}
		else if(getPlayerCurrency(senduid) - this.amount < 0)
		{
			message = "not enough to send.";
			return false;
		}
		else if(getPlayerCurrency(reciveuid) + this.amount > maxValue)
		{
			message = "reciver has to much.";
			return false;
		}
		setPlayerCurrency(senduid, getPlayerCurrency(senduid) - this.amount);
		setPlayerCurrency(reciveuid, getPlayerCurrency(reciveuid) + this.amount);
		return true;
	}

	public String getMessage() {
		return message;
	}

	public String getCurrencyName()
	{
		return currencyName;
	}

	public String getCurrencySymbol()
	{
		return currencySymbol;
	}

	public double getAmount() {
		return amount;
	}

	public double getMinValue()
	{
		return minValue;
	}

	public double getMinUnit()
	{
		return minUnit;
	}

	public double getMaxValue()
	{
		return maxValue;
	}

	public double getMaxUnit()
	{
		return maxUnit;
	}

	public double getDefaultStartValue()
	{
		return defaultStartValue;
	}

	public int getMinimalDecimal()
	{
		return minimalDecimal;
	}
}