package org.forkzone.mc.benis;

import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class Benis extends JavaPlugin implements Listener
{
	private final static String PREFIX = "[BENIS] ";
	CurrencyManager benis;

	@Override
	public void onEnable()
	{
		saveDefaultConfig();
		reloadConfig();
		benis = new CurrencyManager(
				getConfig().getString("CurrencyName", "Benis"),
				getConfig().getString("CurrencySymbol", "B"),
				getConfig().getDouble("MinValue", Double.MIN_VALUE),
				getConfig().getDouble("MaxValue", Double.MAX_VALUE),
				getConfig().getDouble("MinUnit", Double.MIN_NORMAL),
				getConfig().getDouble("MaxUnit", Double.MAX_VALUE),
				getConfig().getDouble("Default", 0),
				getConfig().getInt("Decimal", Double.MIN_EXPONENT));
		getServer().getPluginManager().registerEvents(this, this);
	}

	@Override
	public void onDisable()
	{
		for(String playeruid : benis.getPlayerList())
			getConfig().set("Values." + playeruid, benis.getPlayerCurrency(playeruid));
		saveConfig();
		benis.deleteAllPlayer();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(cmd.getName().equalsIgnoreCase("benis"))
		{
			if(args.length == 0)
				sender.sendMessage(PREFIX + "/" + cmd.getName() + " help");
			else
			{
				if(args[0].equalsIgnoreCase("info"))
				{
					sender.sendMessage(PREFIX + "--- INFO ---");
					sender.sendMessage(PREFIX + "CurrencyName: " + benis.getCurrencyName());
					sender.sendMessage(PREFIX + "CurrencySymbol: " + benis.getCurrencySymbol());
					sender.sendMessage(PREFIX + "MinValue: " + benis.getMinValue());
					sender.sendMessage(PREFIX + "MaxValue: " + benis.getMaxValue());
					sender.sendMessage(PREFIX + "MinUnit: " + benis.getMinUnit());
					sender.sendMessage(PREFIX + "MaxUnit: " + benis.getMaxUnit());
					sender.sendMessage(PREFIX + "Default: " + benis.getDefaultStartValue());
					sender.sendMessage(PREFIX + "Decimal: " + benis.getMinimalDecimal());
				}
				else if(args[0].equalsIgnoreCase("version"))
				{
					sender.sendMessage(PREFIX + "--- PLUGIN ---");
					sender.sendMessage(PREFIX + "Author: basman93");
					sender.sendMessage(PREFIX + "Version: " + this.getDescription().getVersion());
				}
				else if(args[0].equalsIgnoreCase("help"))
				{
					sender.sendMessage(PREFIX + "--- HELP ---");
					sender.sendMessage(PREFIX + "/benis info -- prints info");
					sender.sendMessage(PREFIX + "/benis help -- prints this");
					sender.sendMessage(PREFIX + "/benis version -- prints version");
					sender.sendMessage(PREFIX + "/benis get [player] -- prints balance");
					sender.sendMessage(PREFIX + "/benis send <player> <amount> -- send amount to player");
					if(sender.hasPermission("benis.admin"))
					{
						sender.sendMessage(PREFIX + "--- ADMIN ---");
						sender.sendMessage(PREFIX + "/benis save -- save to config");
						sender.sendMessage(PREFIX + "/benis load -- loads from config");
						sender.sendMessage(PREFIX + "/benis reload -- reload plugin");
						sender.sendMessage(PREFIX + "/benis add <player> <amount> -- add balance to player");
						sender.sendMessage(PREFIX + "/benis set <player> <amount> -- set player balance");
					}
				}
				else if(args[0].equalsIgnoreCase("save") && sender.hasPermission("benis.admin"))
				{
					save();
					sender.sendMessage(PREFIX + "saved to config!");
				}
				else if(args[0].equalsIgnoreCase("load") && sender.hasPermission("benis.admin"))
				{
					load();
					sender.sendMessage(PREFIX + "loaded from config!");
				}
				else if(args[0].equalsIgnoreCase("reload") && sender.hasPermission("benis.admin"))
				{
					save();
					load();
					sender.sendMessage(PREFIX + "reloaded plugin!");
				}
				else if(args[0].equalsIgnoreCase("get"))
				{
					if(args.length == 1 && sender instanceof Player)
						sender.sendMessage(PREFIX + benis.getPlayerCurrency(CurrencyManager.playerToUID(sender)) + " " + benis.getCurrencyName());
					else if(args.length == 1  && !(sender instanceof Player))
						sender.sendMessage(PREFIX + "Player only!");
					else
					{
						Player player = getPlayer(args[1]);
						if(player != null)
							sender.sendMessage(PREFIX + "Player " + player.getName() + " balance: " + benis.getPlayerCurrency(CurrencyManager.playerToUID(player)) + " " + benis.getCurrencyName());
						else
							sender.sendMessage("Error: Player \"" + args[1] + "\" not found.");
					}
				}
				else if(args.length == 3)
				{
					if(args[0].equalsIgnoreCase("add") && sender.hasPermission("benis.admin"))
					{
						if(NumberUtils.isNumber(args[2]))
						{
							Player player = getPlayer(args[1]);
							if(player != null)
							{
								benis.addPlayerCurrency(CurrencyManager.playerToUID(player), Double.parseDouble(args[2]));
								sender.sendMessage(PREFIX + "Added " + args[2] + " " + benis.getCurrencyName() + " to " + player.getName() + " Account.");
							}
							else
								sender.sendMessage("Error: Player \"" + args[1] + "\" not found.");
						}
						else
							sender.sendMessage("Error: \"" + args[2] + "\" is not a number.");
					}
					else if(args[0].equalsIgnoreCase("set") && sender.hasPermission("benis.admin"))
					{
						if(NumberUtils.isNumber(args[2]))
						{
							Player player = getPlayer(args[1]);
							if(player != null)
							{
								benis.setPlayerCurrency(CurrencyManager.playerToUID(player), Double.parseDouble(args[2]));
								sender.sendMessage(PREFIX + "Set " + player.getName() + " " + benis.getCurrencyName() + " to " + args[2]);
							}
							else
								sender.sendMessage("Error: Player \"" + args[1] + "\" not found.");
						}
						else
							sender.sendMessage("Error: \"" + args[2] + "\" is not a number.");
					}
					else if(args[0].equalsIgnoreCase("send"))
					{
						if(sender instanceof Player)
						{
							if(NumberUtils.isNumber(args[2]))
							{
								Player player = getPlayer(args[1]);
								if(player == null)
									sender.sendMessage("Error: Player \"" + args[1] + "\" not found.");
								else if(player.getUniqueId().toString().equalsIgnoreCase(((Player) sender).getUniqueId().toString()))
									sender.sendMessage("Error: Cannot send to yourself.");
								else
								{
									if(benis.transfarCurrency(CurrencyManager.playerToUID(sender), CurrencyManager.playerToUID(player), Double.parseDouble(args[2])))
										sender.sendMessage(PREFIX + "Transfer " + benis.getAmount() + " " + benis.getCurrencyName() + " to " + player.getName() + " Account.");
									else
										sender.sendMessage(PREFIX + "Error: " + benis.getMessage());
								}
							}
							else
								sender.sendMessage("Error: \"" + args[2] + "\" is not a number.");
						}
						else
							sender.sendMessage(PREFIX + "Player only!");
					}
				}
			}
			return true;
		}
		return false;
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent evt)
	{
		String puid = evt.getPlayer().getUniqueId().toString();
		if(getConfig().contains("Values." + puid, true))
		{
			if(!benis.existsPlayer(puid))
				benis.addPlayerCurrency(puid, getConfig().getDouble("Values." + puid, 0D));
		}
		else
		{
			benis.addPlayer(puid);
			getConfig().set("Values." + puid, benis.getDefaultStartValue());
		}
		saveConfig();
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent evt)
	{
		String puid = evt.getPlayer().getUniqueId().toString();
		getConfig().set("Values." + puid, benis.getPlayerCurrency(puid));
		saveConfig();
		benis.deletePlayer(puid);
	}

	private void save()
	{
		for(String playeruid : benis.getPlayerList())
			getConfig().set("Values." + playeruid, benis.getPlayerCurrency(playeruid));
		saveConfig();
	}

	private void load()
	{
		reloadConfig();
		benis = new CurrencyManager(
				getConfig().getString("CurrencyName", "Benis"),
				getConfig().getString("CurrencySymbol", "B"),
				getConfig().getDouble("MinValue", Double.MIN_VALUE),
				getConfig().getDouble("MaxValue", Double.MAX_VALUE),
				getConfig().getDouble("MinUnit", Double.MIN_NORMAL),
				getConfig().getDouble("MaxUnit", Double.MAX_VALUE),
				getConfig().getDouble("Default", 0),
				getConfig().getInt("Decimal", Double.MIN_EXPONENT));
		for(Player player : getServer().getOnlinePlayers())
		{
			String puid = player.getUniqueId().toString();
			if(getConfig().contains("Values." + puid, true))
			{
				if(!benis.existsPlayer(puid))
					benis.setPlayerCurrency(puid, getConfig().getDouble("Values." + puid, 0D));
			}
			else
			{
				benis.addPlayer(puid);
				getConfig().set("Values." + puid, benis.getDefaultStartValue());
				saveConfig();
			}
		}
	}

	private Player getPlayer(String name)
	{
		for(Player buffer : getServer().getOnlinePlayers())
		{
			if(buffer.getName().equalsIgnoreCase(name))
				return buffer;
		}
		return null;
	}
}